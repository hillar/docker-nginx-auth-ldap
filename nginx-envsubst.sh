#!/bin/sh

# set defaults

export LDAP_PROTO=${LDAP_PROTO:-ldap}
export LDAP_CACHE_ENABLED=${LDAP_CACHE_ENABLED:-on} 
export LDAP_CACHE_EXPIRATION_TIME=${LDAP_CACHE_EXPIRATION_TIME:-600000}
export LDAP_CACHE_SIZE=${LDAP_CACHE_SIZE:-1000}
export LDAP_ATTRIBUTE=${LDAP_ATTRIBUTE:-uid}
export LDAP_SCOPE=${LDAP_SCOPE:-sub}
export LDAP_FILTER=${LDAP_FILTER:-(objectClass=posixAccount)}

for template in $(find /etc/nginx -name '*.conf.template'); do
  envsubst "$(printf '${%s} ' $(env | cut -d= -f1))" < $template > ${template%.template}
done
