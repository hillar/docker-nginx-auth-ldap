[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

About
=====

[Docker nginx mainline alpine ](https://github.com/nginxinc/docker-nginx/tree/master/mainline/alpine) + [nginx-auth-ldap](https://github.com/kvspb/nginx-auth-ldap)


### environment

> see [nginx-auth-ldap module example configuration](https://github.com/kvspb/nginx-auth-ldap#example-configuration)

| Parameter | Description |
|-----------|-------------|
|LDAP_PROTO | Proto to use ldap or ldaps |
|LDAP_HOST	| Hostname (and port number) of LDAP Server (e.g. ldapserver.localdomain:389)|
|LDAP_BASE_DN	| Base Distringuished Name (e =dc=hostname,dc=com)|
|LDAP_ATTRIBUTE	| Unique Identifier Attrbiute (e.g. uid)|
|LDAP_SCOPE	| Scope for searching (e.g. sub)|
|LDAP_FILTER	| Define what object that is searched for (e.g. objectClass=posixAccount) |
|LDAP_BINDDN |User to Bind to LDAP (e.g. cn=admin,dc=orgname,dc=org)|
|LDAP_BINDDN_PASSWD| Password for Above Bind User (e.g. password)|
|LDAP_GROUP_ATTRIBUTE	| If searching inside of a group what is the Group Attribute (e.g. uniquemember)|

#### defaults

| Parameter | Default value |
|-----------|-------------|
|LDAP_PROTO | ldap|
|LDAP_CACHE_ENABLED | on |
|LDAP_CACHE_EXPIRATION_TIME | 600000|
|LDAP_CACHE_SIZE | 1000|
|LDAP_ATTRIBUTE | uid|
|LDAP_SCOPE | sub|
|LDAP_FILTER |(objectClass=posixAccount)|




### docker-compose.yml

```
version: '3'
services:
  nginx-auth-ldap:
    image: registry.gitlab.com/hillar/docker-nginx-auth-ldap
    environment:
      LDAP_PROTO: ldaps
      LDAP_HOST: ipa.demo1.freeipa.org
      LDAP_BASE_DN: cn=accounts,dc=demo1,dc=freeipa,dc=org
      LDAP_BINDDN: uid=manager,cn=users,cn=accounts,dc=demo1,dc=freeipa,dc=org
      LDAP_BINDDN_PASSWD: Secret123
 
```

```

$ for user in admin manager helpdesk employee notexists; \
do \
  curl -u $user:Secret123 localhost:8080; \
done

```

```

nginx-auth-ldap_1  | 172.18.0.1 - admin [09/Mar/2019:21:58:22 +0000] "GET / HTTP/1.1" 200 612 "-" "curl/7.43.0" "-"
nginx-auth-ldap_1  | 172.18.0.1 - manager [09/Mar/2019:21:58:22 +0000] "GET / HTTP/1.1" 200 612 "-" "curl/7.43.0" "-"
nginx-auth-ldap_1  | 172.18.0.1 - helpdesk [09/Mar/2019:21:58:23 +0000] "GET / HTTP/1.1" 200 612 "-" "curl/7.43.0" "-"
nginx-auth-ldap_1  | 172.18.0.1 - employee [09/Mar/2019:21:58:23 +0000] "GET / HTTP/1.1" 200 612 "-" "curl/7.43.0" "-"
nginx-auth-ldap_1  | 172.18.0.1 - notexists [09/Mar/2019:21:58:23 +0000] "GET / HTTP/1.1" 401 179 "-" "curl/7.43.0" "-"
nginx-auth-ldap_1  | 2019/03/09 21:58:23 [error] 13#13: *31 http_auth_ldap: Could not find user DN, client: 172.18.0.1, server: localhost, request: "GET / HTTP/1.1", host: "localhost:8080"

```




----


If you are not sure what ldap params to use, here is a tip:

`ldapsearch -h ${*LDAP_HOST*} -D ${*LDAP_BINDDN*} -w ${*LDAP_BINDDN_PASSWD*} -b ${*LDAP_BASE_DN*}`

For example see https://www.freeipa.org/page/Demo

 ```

 ldapsearch -h ipa.demo1.freeipa.org \
            -D uid=manager,cn=users,cn=accounts,dc=demo1,dc=freeipa,dc=org \
            -w Secret123 \
            -b cn=compat,dc=demo1,dc=freeipa,dc=org

 ```





